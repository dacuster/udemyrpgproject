﻿using RPG.Combat;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour
    {
        private NavMeshAgent navMeshAgent;

        private float defaultSpeed = 1f;

        // Start is called before the first frame update
        void Start()
        {
            navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
            defaultSpeed = navMeshAgent.speed;

            return;
        }

        // Update is called once per frame
        void Update()
        {
            GetInput();

            UpdateAnimator();
        }

        private void GetInput()
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                navMeshAgent.speed = defaultSpeed * 2f;
            }
            else
            {
                navMeshAgent.speed = defaultSpeed;
            }

            return;
        }

        public void StartMoveAction(Vector3 _destination)
        {
            GetComponent<Fighter>().Cancel();

            MoveTo(_destination);

            return;
        }

        public void MoveTo(Vector3 _destination)
        {
            navMeshAgent.destination = _destination;
            navMeshAgent.isStopped = false;

            return;
        }

        public void Stop()
        {
            Debug.Log("Stopped");
            navMeshAgent.isStopped = true;

            return;
        }

        private void UpdateAnimator()
        {
            // Get the global velocity.
            Vector3 velocity = navMeshAgent.velocity;

            // Convert global velocity to local.
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);

            // Get forward velocity.
            float speed = localVelocity.z;

            // Set the speed of the animation.
            GetComponent<Animator>().SetFloat("forwardSpeed", speed);

            return;
        }
    }
}
