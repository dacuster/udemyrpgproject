﻿using System;
using UnityEngine;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] private Transform target;

        private float lastMouseX = 0;
        private float lastMouseY = 0;
        private float transformMultiplier = 10f;
        private float maxY = 9.5f;
        private float minY = 2.5f;

        private Vector3 defaultCameraPosition;
        private Quaternion defaultCameraRotation;

        [SerializeField] private bool isFixed = false;

        [SerializeField] private Transform lookAtCamera;

        private void Awake()
        {
            defaultCameraPosition = Camera.main.transform.position;
            defaultCameraRotation = Camera.main.transform.rotation;

            return;
        }

        // Start is called before the first frame update
        void Start()
        {
            target = GameObject.Find("Player").transform;
            lookAtCamera = GameObject.Find("LookAtCamera").transform;
        }

        // Update is called once per frame.
        void LateUpdate()
        {
            ViewTarget();
            CheckInput();

            return;
        }

        private void CheckInput()
        {
            // TODO: Add camera view reset so the position is fixed at 3/4 view.
            if (isFixed)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    FixedRotateCamera(1);
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    FixedRotateCamera(-1);
                }
            }
            else
            {
                // Center mouse button.
                if (Input.GetMouseButtonDown(2))
                {
                    lastMouseX = Input.mousePosition.x;
                    lastMouseY = Input.mousePosition.y;
                }
                else if (Input.GetMouseButton(2))
                {
                    AdjustCameraHeight();
                    FreeRotateCamera();
                    LookAtCamera();
                }
            }

            if (Input.GetKeyDown(KeyCode.V))
            {
                ToggleCameraView();
            }

            return;
        }

        private void LookAtCamera()
        {
            Camera.main.transform.LookAt(lookAtCamera.transform.position);

            return;
        }

        private void ViewTarget()
        {
            transform.position = target.position;

            return;
        }

        // Rotate camera based on mouse movement with center button.
        private void FreeRotateCamera()
        {
            float currentX = Input.mousePosition.x - lastMouseX;

            transform.RotateAround(target.position, Vector3.up, currentX * Time.deltaTime * transformMultiplier);

            lastMouseX = Input.mousePosition.x;

            return;
        }

        private void FixedRotateCamera(int _direction)
        {
            transform.RotateAround(target.position, Vector3.up, 90 * _direction);

            return;
        }

        private void AdjustCameraHeight()
        {
            float currentY = Input.mousePosition.y - lastMouseY;

            Vector3 currentPosition = Camera.main.transform.position;

            // Invert mouse movement.
            currentPosition.y -= currentY * Time.deltaTime;

            // Keep the y position within the adjustment range.
            currentPosition.y = Mathf.Clamp(currentPosition.y, minY, maxY);

            Camera.main.transform.position = currentPosition;

            lastMouseY = Input.mousePosition.y;

            return;
        }

        private void ToggleCameraView()
        {
            isFixed = !isFixed;

            if (isFixed)
            {
                ResetCameraTransform();
            }

            return;
        }

        private void ResetCameraTransform()
        {
            Debug.Log("Position: " + defaultCameraPosition.x + " " + defaultCameraPosition.y + " " + defaultCameraPosition.z);
            Debug.Log("Rotation: " + defaultCameraRotation.x + " " + defaultCameraRotation.y + " " + defaultCameraRotation.z);

            Camera.main.transform.position = defaultCameraPosition;
            Camera.main.transform.rotation = defaultCameraRotation;

            return;
        }
    }
}
