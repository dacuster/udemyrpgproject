﻿using RPG.Movement;
using UnityEngine;


namespace RPG.Combat
{
    public class Fighter : MonoBehaviour
    {
        [SerializeField] private float weaponRange = 2f;

        private Mover mover;

        private Transform target;

        private void Start()
        {
            mover = GetComponent<Mover>();

            return;
        }

        private void Update()
        {
            if (target != null)
            {
                if (!GetIsInRange())
                {
                    mover.MoveTo(target.position);
                }
                else
                {
                    mover.Stop();
                }
            }

            return;
        }

        private bool GetIsInRange()
        {
            return Vector3.Distance(transform.position, target.position) < weaponRange;
        }

        public void Attack(CombatTarget _target)
        {
            target = _target.transform;

            return;
        }

        public void Cancel()
        {
            target = null;

            return;
        }
    }
}